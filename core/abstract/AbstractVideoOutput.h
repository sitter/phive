#ifndef PHONON_ABSTRACTVIDEOOUTPUT_H
#define PHONON_ABSTRACTVIDEOOUTPUT_H

#include <QList>

namespace Phonon {

class OutputEffect;

class AbstractVideoOutput
{
public:
    AbstractVideoOutput();

    void addEffect(OutputEffect *effect);

private:
    QList<OutputEffect *> m_effects;
};

} // namespace Phonon

#endif // PHONON_ABSTRACTVIDEOOUTPUT_H
