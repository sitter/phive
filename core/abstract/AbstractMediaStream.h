#ifndef PHONON_ABSTRACTMEDIASTREAM_H
#define PHONON_ABSTRACTMEDIASTREAM_H

#include <QtCore/QObject>

#include <five_global.h>

namespace Phonon {

class AbstractMediaStream : public QObject
{
    Q_OBJECT
public:
    virtual ~AbstractMediaStream();

protected:
    explicit AbstractMediaStream(QObject *parent = 0);

    // Up to implementation whether this is thread safe
    virtual qint64 size() const = 0;
    virtual bool seekable() const = 0;

//    void writeData(const QByteArray &data);

//    void endOfData();

#warning decl
//    void error(Phonon::ErrorType errorType, const QString &errorString);

    // All the other crap is signaled/slotted to make communicaton 100% async thus
    // allowing for same-thread and other-thread streams.
signals:
    void seekDone();
    void writeData(const QByteArray &);
    void endOfData();
#warning see above
//    error();

protected slots:
    virtual void reset() = 0;
    virtual void needData(quint32 size) = 0;
    virtual void enoughData();
    virtual void seekStream(qint64 position);
};

} // namespace Phonon

#endif // PHONON_ABSTRACTMEDIASTREAM_H
