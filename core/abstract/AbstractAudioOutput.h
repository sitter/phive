#ifndef PHONON_ABSTRACTAUDIOOUTPUT_H
#define PHONON_ABSTRACTAUDIOOUTPUT_H

#include <QList>

namespace Phonon {

class OutputEffect;

class AbstractAudioOutput
{
public:
    AbstractAudioOutput();

    void addEffect(OutputEffect *effect);

private:
    QList<OutputEffect *> m_effects;
};

} // namespace Phonon

#endif // PHONON_ABSTRACTAUDIOOUTPUT_H
