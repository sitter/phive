#-------------------------------------------------
#
# Project created by QtCreator 2012-09-21T08:23:37
#
#-------------------------------------------------

QT       -= gui

TARGET = phononcore
TEMPLATE = lib
VERSION = 5.0.0

DEFINES += PHONON_LIBRARY

#INCLUDEPATH += abstract

SOURCES += \
    VideoDataOutput.cpp \
    AudioDataOutput.cpp \
    abstract/AbstractMediaStream.cpp \
    abstract/AbstractVideoOutput.cpp \
    abstract/AbstractAudioOutput.cpp \
    AudioOutput.cpp \
    BackendCapabilities.cpp \
    Player.cpp \
    Source.cpp \
    Queue.cpp \
    OutputEffect.cpp \
    effects/SubtitleEffect.cpp

HEADERS += \
        five_global.h \
    VideoDataOutput.h \
    AudioDataOutput.h \
    abstract/AbstractMediaStream.h \
    abstract/AbstractVideoOutput.h \
    abstract/AbstractAudioOutput.h \
    AudioOutput.h \
    BackendCapabilities.h \
    Player.h \
    Source.h \
    Queue.h \
    OutputEffect.h \
    effects/SubtitleEffect.h \
    Factory_p.h \
    interfaces/BackendInterface.h \
    interfaces/PlayerInterface.h

unix:!symbian {
    target.path = /usr/local/lib
    INSTALLS += target
}
