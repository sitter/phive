#ifndef PHONON_GLOBAL_H
#define PHONON_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(PHONON_LIBRARY)
#  define PHONONSHARED_EXPORT Q_DECL_EXPORT
#else
#  define PHONONSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // PHONON_GLOBAL_H
