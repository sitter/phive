#ifndef PHONON_AUDIODATAOUTPUT_H
#define PHONON_AUDIODATAOUTPUT_H

#include "AbstractAudioOutput.h"

namespace Phonon {

class AudioDataOutput : public Phonon::AbstractAudioOutput
{
public:
    AudioDataOutput();
};

} // namespace Phonon

#endif // PHONON_AUDIODATAOUTPUT_H
