#ifndef PHONON_SOURCE_H
#define PHONON_SOURCE_H

#include <QString>
#include <QUrl>

#include <AbstractMediaStream.h>

namespace Phonon {

class Source
{
public:
    enum Type {
        Invalid = -1,
        LocalFile,
        Url,
        Disc,
        Stream,
        CaptureDevice,
        Empty,
        AudioVideoCapture
    };

    Source();
    Source(const QString &fileName); //krazy:exclude=explicit
    Source(const QUrl &url); //krazy:exclude=explicit
    Source(DiscType discType, const QString &deviceName = QString()); //krazy:exclude=explicit;
    Source(AbstractMediaStream *stream);
    Source(QIODevice *ioDevice); //krazy:exclude=explicit

    ~Source();

    Source(const Source &rhs);
    Source &operator=(const Source &rhs);
    bool operator==(const Source &rhs) const;

#warning fucking autodelete
//    void setAutoDelete(bool enable);
//    bool autoDelete() const;

    Type type() const;
    QString fileName() const;

    QUrl url() const;
    DiscType discType() const;

#warning questoinmark!
    QString deviceName() const;

    AbstractMediaStream *stream() const;

#warning capture whoop whoop



    enum Attribute {
        AudioTrack
    };

    bool configure(Attribute attr, QVariant value);
};

} // namespace Phonon

#endif // PHONON_SOURCE_H
