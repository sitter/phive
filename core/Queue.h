#ifndef PHONON_QUEUE_H
#define PHONON_QUEUE_H

#include <QList>
#include <QObject>

#include "Source.h"
#include "Player.h"

namespace Phonon {

class Player;
class Source;

class Queue : public QObject
{
    Q_OBJECT
    public:
        Queue(Player *player)
            : m_player(player)
        {
            connect(player, SIGNAL(aboutToFinish()), this, SLOT(handleAboutToFinish()));
        }

        /**
         * Inserts the item before the 'before' source.
         * Empty source means at the end.
         */
        void enqueue(const Source &source, const Source &before=Source());

        /**
         * Removes the item from the queue
         */
        Source dequeue(const Source &source = Source());

        /**
         * clears the queue
         */
        void clear();

        /**
         * Reorders elements in the queue.
         * When target is the empty source, it moves the source to the end
         */
        void moveBefore(const Source &source, const Source &target);

        QList<Source> items() const;

        Queue &operator<<(const Source &source);

    private slots:
        void handleAboutToFinish() {
            m_player->setSource(m_items.takeFirst());
        }

    private:
        Player *m_player;
        QList<Source> m_items;
};

} // namespace Phonon

#endif // PHONON_QUEUE_H
