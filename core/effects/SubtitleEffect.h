#ifndef PHONON_SUBTITLEEFFECT_H
#define PHONON_SUBTITLEEFFECT_H

#include <OutputEffect.h>

namespace Phonon {

class SubtitleEffect : OutputEffect
{
public:
    SubtitleEffect();
};

} // namespace Phonon

#endif // PHONON_SUBTITLEEFFECT_H
