#ifndef PHONON_AUDIOOUTPUT_H
#define PHONON_AUDIOOUTPUT_H

#include <QString>

#include "AbstractAudioOutput.h"

namespace Phonon {

class AudioOutput : public Phonon::AbstractAudioOutput
{
public:
    explicit AudioOutput(Phonon::Category category, QObject *parent = 0);
    explicit AudioOutput(QObject *parent = 0);

    QString name() const;
    qreal volume() const;
    qreal volumeDecibel() const;

    Phonon::Category category() const;
    AudioOutputDevice outputDevice() const;
    bool isMuted() const;

public Q_SLOTS:
    void setName(const QString &newName);
    void setVolume(qreal newVolume);
    void setVolumeDecibel(qreal newVolumeDecibel);
    bool setOutputDevice(const Phonon::AudioOutputDevice &newAudioOutputDevice);
    void setMuted(bool mute);

Q_SIGNALS:
    void volumeChanged(qreal newVolume);
    void mutedChanged(bool);
    void outputDeviceChanged(const Phonon::AudioOutputDevice &newAudioOutputDevice);
};

} // namespace Phonon

#endif // PHONON_AUDIOOUTPUT_H
