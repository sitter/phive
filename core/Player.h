#ifndef PHONON_PLAYER_H
#define PHONON_PLAYER_H

#include <QObject>
#include "Source.h"

namespace Phonon {

class AbstractAudioOutput;
class AbstractVideoOutput;

enum State {
    Stopped,
    Paused,
    Playing
};

class Player : public QObject
{
    Q_OBJECT
public:
    explicit Player(QObject *parent = 0);
    
    void play();
    void pause();
    void stop();

    void seek(qint64 position);

    qint64 currentTime() const;
    qint64 totalTime() const;
    bool isSeekable() const;

    State state() const;

    Source source() const;
    void setSource(const Source &newSource);

    bool addOutput(AbstractAudioOutput *audioOutput);
    bool addOutput(AbstractVideoOutput *videoOutput);

signals:
    void stateChanged(State newState, State oldState);
    void finished();
#warning buffering

private:
    QList<AbstractAudioOutput *> m_audioOutputs;
    QList<AbstractVideoOutput *> m_videoOutputs;

#warning do we want hasVideo or require a VO to detect that?
#warning metadata missing
#warning remainingTime interface?
#warning ticking missing
};

} // namespace Phonon

#endif // PHONON_PLAYER_H
