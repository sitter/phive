#ifndef PHONON_FACTORY_P_H
#define PHONON_FACTORY_P_H

class QObject;

namespace Phonon {

class BackendInterface;

namespace Factory
{
#warning some grand unified enum would be cool as this is also needed by the backend interface *and* the backendcapabilities
    enum ObjectType {
        Player
    };

    BackendInterface *backend(bool createWhenNull = true);
    QObject *createBackendObject(ObjectType, QObject *parent = 0);
} // namespace Factory

} // namespace Phonon

#endif // PHONON_FACTORY_P_H
