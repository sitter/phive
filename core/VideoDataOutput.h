#ifndef PHONON_VIDEODATAOUTPUT_H
#define PHONON_VIDEODATAOUTPUT_H

#include <QtCore/QObject>

#include "AbstractVideoOutput.h"

namespace Phonon {

class VideoFrame {
public:
    enum Format{};
};

#warning frame shared?
#warning opengl frame?

class VideoDataOutput : public QObject, public AbstractVideoOutput
{
    Q_OBJECT
public:
    VideoDataOutput();

    virtual QList<VideoFrame::Format> supportedFormats() const;
    virtual void setSupportedFormats(const QList<VideoFrame::Format> &formats);

Q_SIGNALS:
    void frameReady(VideoFrame frame);

        // order matters -> qlist (order = preference raiting)
        void offering(const QList<VideoFrame::Format> &offers);
        void choose(VideoFrame::Format format); // final choice

protected Q_SLOTS:
    virtual void showFrame() { Q_EMIT frameReady(*frame()); }
    virtual void reset();

#warning default impl
        virtual void negotiateFormat() { Q_EMIT offering(supportedFormats()); }
        // backend preferred choice
        virtual void wantFormat(VideoFrame::Format format) { Q_EMIT choose(format); }

protected:
    void lock();
    bool tryLock(int timeout = 0);
    void unlock();
    VideoFrame *frame() const;
};

} // namespace Phonon

#endif // PHONON_VIDEODATAOUTPUT_H
