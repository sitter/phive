#ifndef PHONON_BACKENDCAPABILITIES_H
#define PHONON_BACKENDCAPABILITIES_H

#include <five_global.h>

namespace Phonon {

class BackendCapabilities
{
//    Q_FLAGS(Capabilities)
public:
    enum Capability {
        AbstractMediaStream,
        AudioDataOutput,
        AudioOutput,
        MediaObject,
        VideoDataOutput
    };
//    Q_DECLARE_FLAGS(Capabilities, Capability)

    BackendCapabilities();

//    Capabilities capabilties();
};

} // namespace Phonon

#endif // PHONON_BACKENDCAPABILITIES_H
