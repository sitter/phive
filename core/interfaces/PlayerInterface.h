#ifndef PHONON_PLAYERINTERFACE_H
#define PHONON_PLAYERINTERFACE_H

namespace Phonon {

class PlayerInterface
{
public:
    virtual ~PlayerInterface() {}

    virtual void play() = 0;
    virtual void pause() = 0;
    virtual void stop() = 0;

    virtual void seek(qint64 position) = 0;

    virtual qint64 currentTime() const = 0;
    virtual qint64 totalTime() const = 0;
    virtual bool isSeekable() const = 0;

    virtual State state() const = 0;

    virtual Source source() const = 0;
    virtual void setSource(const Source &newSource);

    virtual bool addOutput(AbstractAudioOutput *audioOutput) = 0;
    virtual bool addOutput(AbstractVideoOutput *videoOutput) = 0;

#warning signals!?!?!
};

} // namespace Phonon

Q_DECLARE_INTERFACE(Phonon::PlayerInterface, "phonon.kde.org.PlayerInterface/5")

#endif // PHONON_PLAYERINTERFACE_H
