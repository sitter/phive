#ifndef PHONON_BACKENDINTERFACE_H
#define PHONON_BACKENDINTERFACE_H

#include <QList>
#include <QVariant>

namespace Phonon {

class BackendInterface
{
public:
#warning see warning in factory header
    enum Class {
        PlayerClass
    };

    virtual ~BackendInterface() {}

    virtual QObject *createObject(Class classType,
                                  QObject *parent,
                                  const QList<QVariant> &args = QList<QVariant>()) = 0;
};

}

#warning note should we bump all interfaces? perhaps use 5.0? seems somewhat tiresome if the interface version is completely random for every interface
Q_DECLARE_INTERFACE(Phonon::BackendInterface, "phonon.kde.org.BackendInterface/5")

#endif // PHONON_BACKENDINTERFACE_H
